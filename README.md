# CANsnoot

Sniffing a CAN bus with the goal of replaying the remote start signal.  Just notes for now.

## Hardware Setup

 * Raspberry Pi 4
 * [Waveshare RS485 CAN hat](https://www.waveshare.com/wiki/RS485_CAN_HAT)
 * Two dupont wires
 * USB Ethernet dongle + ethernet cable


SPI must be enabled on the Pi and `dtoverlay` used to enabled the CAN hat.

In `/boot/config.txt` add the following lines then reboot

```
dtparam=spi=on
dtoverlay=mcp2515-can0,oscillator=12000000,interrupt=25,spimaxfrequency=2000000
```


You should now have a network device listed as `can0`.  Confirm this with `ip addr | grep can`

```
pi@raspberrypi:~ $ ip addr | grep can
3: can0: <NOARP,ECHO> mtu 16 qdisc noop state DOWN group default qlen 10
    link/can 
```

Issue the following commands to configure the device for capturing live traffic.  

```
sudo ip link set can0 up type can bitrate 500000
sudo ifconfig can0 txqueuelen 6553
```

Running `ip addr` again should show the device now enabled with the queue length set to 6553

```
3: can0: <NOARP,UP,LOWER_UP,ECHO> mtu 16 qdisc pfifo_fast state UP group default qlen 65536
```

Install `can-utils` with `sudo apt install can-utils`.

## Network Setup

It is advised that you set a static IP for both the pi and laptop being used to interface with the pi. `nmtui` can be used, or the `networkmanager` GUI, or the command line.  The net result should be the ability to ssh into the pi over ethernet.

```
$ ip address flush dev eth1
$ ip route flush dev eth1
$ ip address add 192.168.6.66/24 brd + dev eth1
$ ip route add 192.168.6.1 dev eth1
$ ip route add default via 192.168.6.1 dev eth1
$ ip address show dev eth1

[...]
  inet 192.168.6.66/24 brd 192.168.6.255 scope global eth1
[...]
```

## Capturing CAN traffic

### OBD Wiring
Generally speaking, all OBD-II ports follow a standard pinout, and the only ones we're concerned with are CAN H and CAN L, pins 6 and 14, respectively.

 ![OBD Pinout](obd_pinout.png "Standard OBD Pinout")

 Wire CAN H to pin 6 and CAN L to pin 14.

  ![Hardware](hardware.jpg "Hardware wiring")

---

### Observing Live Traffic

 1. Boot the pi
 2. Confirm that you can talk to the pi.  Ping it, ssh in..
 3. On the pi run `candump can0`.  You will not see any traffic yet.
 4. Turn the key to the `ON` position in your car, but do not start the engine.  You should see traffic from `candump` now.
 5. *Optional* Crank the engine, rev it, use your turn signals. Do anything that might elicit a message in the CAN bus for inspection later.

  ![Live Capture](live_capture.jpg "CANdump live traffic")

### Capturing Live Traffic

Follow the same steps as above, but in step `3` run `candump -L can0 > traffic.log`.  It is recommended that you launch a `tmux` session, or open a second ssh session, and `tail -f traffic.log` to ensure traffic is flowing.  

  ![Live Capture](logfile.jpg "CANdump live traffic")

## Replaying and Inspecting CAN traffic

The `.log` file can be replayed with `canplayer -I traffic.log`, or the raw contents inspected with `less`, `cat`, or whatever text viewer you prefer.  

Once I find a way to replay traffic on a machine without a CAN interface I will update it here.  I've included a live capture example.